const product = require("./product");
const category = require("./category");
const banner = require("./banner");
const slider = require("./slider");
const signup = require('./signup');
const login = require("./login");
const cart = require("./cart");
module.exports = {
  product,
  category,
  slider,
  login,
  banner,
  signup,
  cart
  
};
