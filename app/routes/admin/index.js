const product = require('./product');
const uploadImage = require('./uploadImage');
const category = require('./category');
const banner = require('./banner');
const slider = require('./slider');
const login = require('./login');
const image = require('./image');

module.exports = {
  image,
  uploadImage,
  product,
  category,
  slider,
  banner,
  login,
};
