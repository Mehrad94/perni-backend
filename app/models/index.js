const Category = require('./Category');
const Product = require('./Product');
const Slider = require('./Slider');
const Banner = require('./Banner');
const User = require('./User');
const Admin = require('./Admin');
const Verification = require('./Verification');
const Image = require('./Image');

const models = {
  Category,
  Product,
  Slider,
  Banner,
  User,
  Verification,
  Admin,
  Image,
};

module.exports = models;
