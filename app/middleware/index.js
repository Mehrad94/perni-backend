const userAuthentication = require("./userAuthentication");
const adminAuthentication = require("./adminAuthentication");

module.exports = {
  userAuthentication,adminAuthentication
};
